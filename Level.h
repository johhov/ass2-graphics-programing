#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>

#include "ScreenObject.h"
#include "ModelHandler.h"
#include "TextureHandler.h"

class Level
{
public:
	Level(std::string filepath, ModelHandler* modelHandler, TextureHandler* textureHandler);

	std::vector<ScreenObject>* getWalls();
	ScreenObject* getPlane();
	glm::vec2 getMapSize();
	glm::vec3 getStartPos();
	int getScale();
private:
	std::vector<std::vector<int>> map;
	std::vector<ScreenObject> walls;
	ScreenObject plane;

	glm::vec2 mapSize;

	glm::vec3 pacManStartPos;

	//Dirty magic number based on the model data. (It is 2 across in model space.)
	//Should probably be made into part of the model file...
	int scale = 2; 
	ModelHandler* modelHandler;
	TextureHandler* textureHandler;
	void loadMap(std::string filepath); //Loads map data from file
	void createWalls();	//This should create a ScreenObject for each wall segment. 
};