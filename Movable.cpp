#include "Movable.h"
#include <iostream>


/**
 * [Movable::move description]
 * @param collidables [description]
 */
void Movable::move(Level* currentLevel, float deltaTime) 
{
	glm::vec3 deltaPosition;
	deltaPosition = m_movementDirection*(m_speed*deltaTime);
	m_position = m_position + (m_movementDirection*(m_speed*deltaTime));
	
	handleCollision(currentLevel);
	std::cout << m_position.y << '\n';
	//{
		//std::printf("We are moving\n");
	//}
	
	//we have a bug that makes pacman move in a not smooth way.
	//std::cout << std::endl;
	
	m_modelMatrix = glm::translate(glm::mat4(1.0f), m_position);
	m_modelMatrix = glm::scale(m_modelMatrix, m_scale);

}

/**
 * This should rotate the object so that it faces in a specific direction.
 * This will be usefull with the spotlight.
 * @param direction The new direction
 */
void Movable::rotate(glm::vec3 direction)
{
	//m_direction = direction;
}

bool Movable::handleCollision(Level* currentLevel)
{
	bool collision = false;

	for(auto collidable : (*currentLevel->getWalls()))
	{
		if(checkCollisionWith(&collidable))
		{
			resolveCollision(currentLevel, &collidable);
			collision = true;
		}
	}

	if(collision)
	{
		//setDirection(Movable::direction::NONE);
		setDirection(m_oldDirection);
	}

	m_oldDirection = Movable::direction::NONE;
	m_oldMovementDirection = glm::vec3(0.0f, 0.0f, 0.0f);

	return collision;
}

bool Movable::checkCollisionWith(ScreenObject* collidable)
{
	glm::vec3 collidablePos = collidable->getPosition();
	glm::vec3 collidableBox = collidable->getHitbox();
	
	if(	(m_position.x+m_hitbox.x/2 <= collidablePos.x-collidableBox.x/2 || m_position.x-m_hitbox.x/2 >= collidablePos.x+collidableBox.x/2) ||
		(m_position.z+m_hitbox.z/2 <= collidablePos.z-collidableBox.z/2 || m_position.z-m_hitbox.z/2 >= collidablePos.z+collidableBox.z/2)) 
	{
		return false;
	}

	return true;
}

void Movable::resolveCollision(Level* currentLevel, ScreenObject* collidable) 
{
	glm::vec3 collidablePos = collidable->getPosition();
	glm::vec3 collidableBox = collidable->getHitbox();
	
	glm::vec3 newPosition = m_position;

	float col;
	float thi;

	switch(m_direction)
	{
		case(DOWN):
			col = (collidablePos.z-collidableBox.z/2);
			thi = (m_position.z+m_hitbox.z/2);
			newPosition.z = (m_position.z - (thi-col));
			break;
		case(LEFT):
			col = (collidablePos.x+collidableBox.x/2);
			thi = (m_position.x-m_hitbox.x/2);
			newPosition.x = (m_position.x + (col-thi));
			break;
		case(UP):
			col = (collidablePos.z+collidableBox.z/2);
			thi = (m_position.z-m_hitbox.z/2);
			newPosition.z = (m_position.z + (col-thi));
			break;
		case(RIGHT):
			col = (collidablePos.x-collidableBox.x/2);
			thi = (m_position.x+m_hitbox.x/2);
			newPosition.x = (m_position.x - (thi-col));
			break;
		case(NONE):
			//printf("Error!: No direction during collision. This should not happen.\n");
			break;
		default:
		{
			printf("Error!: The direction is not valid.\n");
		}
	}

	m_position = newPosition;
}

void Movable::setDirection(Movable::direction direction) 
{
	m_oldMovementDirection = m_movementDirection;
	m_oldDirection = m_direction;

	switch(direction) 
	{
		case(UP):
			m_movementDirection = glm::vec3(0.0f, 0.0f, -1.0f);
			m_direction = direction;
			break;
		case(LEFT):
			m_movementDirection = glm::vec3(-1.0f, 0.0f, 0.0f);
			m_direction = direction;
			break;
		case(DOWN):
			m_movementDirection = glm::vec3(0.0f, 0.0f, 1.0f);
			m_direction = direction;
			break;
		case(RIGHT):
			m_movementDirection = glm::vec3(1.0f, 0.0f, 0.0f);
			m_direction = direction;
			break;
		case(NONE):
			m_movementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
			m_direction = direction;
			break;
		default:
			printf("Error!: The direction is not valid.\n");
	}
}

void Movable::setSpeed(float speed) 
{
	m_speed = speed;
}

glm::vec3 Movable::getDirection()
{
	return m_movementDirection;
}