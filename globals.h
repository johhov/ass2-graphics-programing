#pragma	once

#include <vector>
#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include "ScreenObject.h"
#include "Level.h"

extern const char gWINDOW_CONFIGURATION_FILE[];
extern const char gLEVELS_FILE[];
extern const char gTEXTURE_FILE[];

extern bool gRunning;
extern float gFpsGoal;

extern std::vector<Level> gLevels;
extern std::vector<GLuint> gTextures;