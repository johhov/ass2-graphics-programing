#pragma	once

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>

#include "Camera.h"
#include "ModelHandler.h"
#include "ShaderHandler.h"
#include "TextureHandler.h"


class ScreenObject
{
public:
	ScreenObject() {};
	ScreenObject(Model* model, glm::vec3 position, glm::vec3 hitbox, glm::vec3 scale, std::string name, TextureHandler* textureHandler);
	~ScreenObject() {};

	void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, glm::vec3 position, glm::vec3 direction);
	void update();

	glm::mat4 getModelMatrix();
	glm::vec3 getPosition();
	glm::vec3 getHitbox();

	void setModel(Model* model);
	void setModelMatrix(glm::mat4 modelMatrix);

	void addTextures(GLuint& texture);
	void updatePosition();

protected:
	glm::mat4 		m_modelMatrix;
	glm::vec3		m_position;
	glm::vec3		m_hitbox;
	GLuint 			textureID;

private:
	Model* 			m_model;
	TextureHandler* textureHandler;
	std::string 	textureName;

	std::vector<GLuint> textures;
};