#include "Level.h"
#include "WindowHandler.h"
#include "iostream"
Level::Level(std::string filepath, ModelHandler* modelHandler, TextureHandler* textureHandler)
{
	this->modelHandler = modelHandler;
	this->textureHandler = textureHandler;
	loadMap(filepath);
	createWalls();
}

/* 
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space.
2 represents Pac Mans start position.
*/
void Level::loadMap(std::string filepath)
{
	int x, y, temp;
	FILE* file = fopen(filepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &x, &y);
	mapSize.x = x;
	mapSize.y = y;
	
	glm::vec2 screenSize = WindowHandler::getInstance().getScreenSize();

	for(int i = 0; i<y; i++)
	{
		std::vector<int> row;
		for(int j = 0; j<x; j++)
		{
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createWalls()
{
	glm::vec3 pos;
	int rows = map.size();
	int cells = 0;
	pos.y = 0;			//sets position.y in the world to 0, to make sure all cubes are on the same field
	for(int i = 0; i<rows; i++) 
	{
		cells = map[i].size();

		for(int j = 0; j<cells; j++) 
		{
			if(map[i][j] == 1) 
			{


				pos.x = i * 2;
				pos.z = j * 2;

				ScreenObject wall(modelHandler->getModel("cube"),pos, glm::vec3(2.f), glm::vec3(1.f,1.f,1.f), "wall", textureHandler);
				walls.push_back(wall);
			}
			if(map[i][j] == 2)
			{
				pacManStartPos.x = i*2;
				pacManStartPos.z = j*2;
			}
		}
	}
	plane = ScreenObject(modelHandler->getModel("cube"), glm::vec3(31.f,-1.f,31.f), glm::vec3(0,0,0), glm::vec3(31.f,1.f,31.f), "plane", textureHandler);
}

std::vector<ScreenObject>* Level::getWalls() {
	return &walls;
}
ScreenObject* Level::getPlane()
{
	return &plane;
}

glm::vec2 Level::getMapSize() {
	return mapSize;
}

glm::vec3 Level::getStartPos() {
	return pacManStartPos;
}

int Level::getScale() {
	return scale;
}